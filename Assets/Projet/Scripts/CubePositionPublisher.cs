using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Geometry;
using System.Collections.Generic;

public class CubePositionPublisher : MonoBehaviour
{
    // pour la gestion de ROS
    [SerializeField]
    private string topicName = "/cube_position";
    private ROSConnection ros;
    private float publishMessageFrequency = 0.5f;
    private float timeElapsed;

    [SerializeField]
    private GameObject robotBaseLink;

    public GameObject robotEffector;

    // pour la gestion des couleurs
    public float distancemin = 0.4f;
    public Material targetMaterial;
    private Material originalMaterial;
    private Renderer closestCubeRenderer;


    private void Start()
    {
        ros = ROSConnection.GetOrCreateInstance();
        ros.RegisterPublisher<PoseMsg>(topicName);
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed > publishMessageFrequency)
        {
            GameObject[] cubes = GameObject.FindGameObjectsWithTag("cube");

            float closestDistance = Mathf.Infinity;
            GameObject closestCube = null;

            foreach (GameObject cube in cubes)
            {
                float distance = Vector3.Distance(cube.transform.position, robotEffector.transform.position);

                if (distance < closestDistance)
                {
                    closestCube = cube;
                    closestDistance = distance;

                    // on r�cup�re la position du cube par rapport � la base du robot
                    Vector3 relativePosition = robotBaseLink.transform.InverseTransformPoint(cube.transform.position);
                    Quaternion relativeRotation = Quaternion.Inverse(robotBaseLink.transform.rotation) * cube.transform.rotation;

                    // On convertit ces donn�es en messages ROS en les convertissant dans le rep�re right-handed de ROS 
                    PoseMsg poseMsg = new PoseMsg();
                    poseMsg.position = new PointMsg(relativePosition.z, -relativePosition.x, relativePosition.y);
                    poseMsg.orientation = new QuaternionMsg(-relativeRotation.z, relativeRotation.x, -relativeRotation.y, relativeRotation.w);

                    // on publie et on r�initialise le temps 
                    ros.Publish(topicName, poseMsg);


                    if (closestDistance < distancemin)
                    {
                        if (closestCubeRenderer == null)
                        {
                            closestCubeRenderer = closestCube.GetComponent<Renderer>();
                            originalMaterial = closestCubeRenderer.material;
                        }

                        closestCubeRenderer.material = targetMaterial;
                    }
                    else
                    {
                        if (closestCubeRenderer != null)
                        {
                            closestCubeRenderer.material = originalMaterial;
                            closestCubeRenderer = null;
                            originalMaterial = null;
                        }
                    }

                }
            }

            timeElapsed = 0;
            
        }
    }
}
