# Aperçu global du projet

Ce projet Unity comporte :
- la visualisation du robot Panda, superposé au robot réel par le package gérant les QR Codes.
- des objets d'intérêt virtuels sous forme de cubes. 
- le package de connexion avec ROS 

Il souscrit au topic /panda/joint_states de ROS pour mobiliser le robot, il nécessite donc un PC Linux qui fait tourner ROS avec un publisher de joint_states. 


## Lancer le projet 

(si besoin d'installer Unity, voir section Installation d'Unity plus bas)

- Télécharger ce package sous forme d'un fichier zip puis le décompresser

- Ouvrir ce projet dans Unity Hub

- Dans la barre de recherche de la fenêtre "Projet", chercher "MainScene" et double-cliquer pour afficher le robot. 

- Dans File > Build Settings : changer la plateforme vers Universal Windows Platform et changer l'architecture vers "ARM64"


## Connexion à ROS

### Côté Windows 

Pour le connecter à votre ordinateur Linux sur lequel vous faites tourner ROS, dans la barre d'outils de Unity, aller dans Robotics > ROS Settings : modifier le "ROS IP adress" pour l'adresse IP de votre ordinateur. 

### Côté Linux

- Il nécessite un topic actif en terme de joint_states, et un URDF classique de panda_arm sans table. 

- Il nécessite d'ajouter le package [ROS TCP Endpoint](https://github.com/Unity-Technologies/ROS-TCP-Endpoint) dans le workspace ROS : 


```bash
cd panda_capacity_ws/src
git clone https://github.com/Unity-Technologies/ROS-TCP-Endpoint.git
```

    - dans le fichier launch de ce package (endpoint.launch), modifier la valeur de "tcp-ip" pour l'adresse IP de la machine utilisée (obtenue grâce à "\$ ifconfig" dans le terminal, section "inet") et la valeur de "tcp_port" à 10000 (valeur présente dans Unity > Robotics > ROS Settings > ROS Port)

    - copier-coller l'intérieur de ce fichier launch dans le fichier launch utilisé


Ce projet ne nécessite pas de node supplémentaire ROS pour tourner. Cependant, il fonctionne avec le node cube_suscriber présent dans le package ROS [panda_unity](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity), qui récupère les coordonnées du cube le plus proche de l'effecteur, dans le repère de référence de la base du robot. 


## Visualisation sur l'Hololens 

Pour afficher la scène de Unity sur l'Hololens : 2 possibilités, nécessitant toutes les deux que le PC Windows et l'Hololens soient connectés au __même__ réseau WiFi. 

### Via l'Holographic Remoting

- Sur l'Hololens : afficher le menu Démarrer. Aller dans "All Apps" et cliquer sur Holographic Remote. Un cube apparaît, cliquer sur le logo Play. L'adresse IP de l'appareil apparaît alors. 

- Du côté d'Unity : faire Mixed Reality > Remoting > Holographic Remoting for Play Mode. Inscrire l'adresse IP de l'Hololens, puis appuyer sur "Enable Holographic Remoting for Play Mode". Puis appuyer sur le logo Play au dessus de la scène. 

### Via la construction de l'application sur l'Hololens 

- Dans Unity > File, cliquer sur Build Settings (vérifier que ce soit bien sur la plateforme UWP) puis sur Build en bas à droite de la fenêtre qui s'ouvre : une fenêtre d'enregistrement s'ouvre, créer un nouveau dossier Builds et le sélectionner. 

- Une fois que la compilation est terminée, la fenêtre s'ouvre : double-cliquer sur PandaCapacityAR.sln. 
- Visual Studio 2019 s'ouvre : 
    - Sur la barre d'outils du haut, modifier "Debug" en "Release" et "ARM" en "ARM64". 
    - Aller dans Project > Properties. Dans Configuration Properties > Debugging, ajouter le machine name (172.16.0.183). 
    - Aller dans la barre d'outils Debug > Start without debugging (avec l'Hololens allumé) : l'application s'ouvrira spontanément à la fin de la procédure. 



# Contenu technique du projet 

### Organisation dans Unity 

L'organisation dans Unity (dans la fenêtre Hierarchy) : 
- le ROS Manager : il contient le script subscriber aux joint_states

- le QRCodeManager : il contient les scripts permettant le tracking des QRCodes et de déterminer leurs coordonnées dans le repère de Unity. 

- le RootQRCode : il s'agit du GameObject parent de toutes les modalités de visualisation. Son script "MoveRobot.cs" récupère les coordonnées du QRCode repéré et se les attribue automatiquement. 

    - le panda : formé via un URDF, il est placé par une transformation via le script MoveRobot également et il se configure grâce au ROS Manager par le script JointSubscriber. 

    - les cubes


Un GameObject "Cubes" sans représentation graphique regroupe les différents cubes, et est associé au script CubePositionPublisher qui permet :
 - de publier en message ROS les coordonnées du cube le plus proche de l'effecteur, dans le repère de la base du robot (coordonnées converties en repère right-handed)

 - de modifier la couleur du cube le plus proche de l'effecteur si celui-ci est à un certain seuil de distance


 ### Comment ça marche

 Les cubes ont le tag "cube" (à faire manuellement si jamais vous voulez en ajouter : dans la fenêtre Inspector du cube, modifier le tag sous le nom). Le script cherche ensuite tous les objets de la scène qui ont ce tag et calcule la distance de l'effecteur avec chacun. 


 ### Modification graphique 

 - Pour ne plus voir le robot : dans la fenêtre Hierarchy, cliquer sur RA > MixedReality Playspace > Main Camera. Dans la fenêtre Inspector, dans la rubrique Camera, aller dans Culling Mask : décocher IgnoreRayCast (qui est le layout du Panda, tout à fait modifiable)

 - Pour ne plus voir les cubes : dans la fenêtre Hierarchy, cliquer sur RootQRCode > Cubes, puis dans la fenêtre Inspector, décocher la case à côté du nom (cela décochera tous les cubes children) 

 - Pour modifier leur aspect initial : dans chaque cube, dans la fenêtre Inspector, rubrique MeshRenderer : modifier  Materials

 - Pour modifier l'aspect à l'approche de l'effecteur : dans le GameObject Cubes, dans la fenêtre Inspector, dans la rubrique du Script, modifier le TargetMaterial

 - Possibilité également de changer le seuil de distance dans cette même fenêtre ou directement dans le script




# Installation de Unity

- Installation de [Unity Hub](https://unity.com/fr/download) après avoir fait un compte (gratuité si étudiant). 

- Installation de [Unity Editor](https://unity.com/releases/editor/archive) version 2021.3.18, disponible dans les archives. 

- Installation de [Universal Windows Platform](download.unity3d.com/download_unity/3129e69bc0c7/TargetSupportInstaller/UnitySetup-Universal-Windows-Platform-Support-for-Editor-2021.3.18f1.exe) pour cette version de Unity Editor (sinon aller dans Unity Editor > Fichier > Build Settings > Universal Windows Platform puis "Open Download Page")

- Installation de [NuGet](https://www.nuget.org/downloads) (plugin d'Unity, gestionnaire de packages)

- Installation du [MixedRealityToolKit](https://www.microsoft.com/en-us/download/details.aspx?id=102778), qui permet de créer des applications de réalité augmentée. 

- Installation de [MixedRealityQR](https://www.nuget.org/Packages/Microsoft.MixedReality.QR) pour la reconnaissance des QR (qui est un package Nuget) via la barre d'outils NuGet > Manage NuGet Packages puis taper "Microsoft.MixedReality.QR" dans la barre de recherche et cliquer sur install 

